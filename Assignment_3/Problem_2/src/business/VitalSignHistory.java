/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class VitalSignHistory {

    private ArrayList<VitalSign> vitalsignlist;
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
    private Person person;

    public VitalSignHistory() {
        vitalsignlist = new ArrayList<VitalSign>();
    }

    public ArrayList<VitalSign> getVitalsignlist() {
        return vitalsignlist;
    }

    public VitalSign addVitalSign() {
        VitalSign vitalSign = new VitalSign();
        vitalsignlist.add(vitalSign);
        return vitalSign;
    }

    public void deleteVitalSign(VitalSign vitalSign) {
        vitalsignlist.remove(vitalSign);
    }

    public void clearVitalSign() {
        vitalsignlist.clear();
    }

}
