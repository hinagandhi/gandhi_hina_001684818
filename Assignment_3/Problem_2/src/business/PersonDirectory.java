/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class PersonDirectory {

    private ArrayList<Person> personDirectory;
    private Person person;

    public PersonDirectory() {
        personDirectory = new ArrayList<>();
    }

    public ArrayList<Person> getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(ArrayList<Person> personDirectory) {
        this.personDirectory = personDirectory;
    }

    public Person addPerson() {
        person = new Person();
        personDirectory.add(person);
        return person;
    }

    public Person searchPerson(String phoneNumber) {
        for (Person p : personDirectory) {
            if (p.getPhoneNumber().equalsIgnoreCase(phoneNumber)) {
                return p;
            }
        }
        return null;
    }

    public void deletePerson(Person person) {
        personDirectory.remove(person);
    }

    public Person searchPatient(String key) {
        for (Person p : personDirectory) {
            if (p.getPatient().getPatientID() != null) {
                if (p.getPatient().getPatientID().equalsIgnoreCase(key)) {
                    return p;
                }
            } else {
                return null;
            }
        }
        return null;
    }
}
