/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Patient {

    private String patientID;
    private String primarydocname;
    private String preferredpharmacy;
    private VitalSignHistory vitalSignHistory;

    public Patient() {
        vitalSignHistory = new VitalSignHistory();
    }

    public VitalSignHistory getVitalsignhistory() {
        return vitalSignHistory;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getPrimarydocname() {
        return primarydocname;
    }

    public void setPrimarydocname(String primarydocname) {
        this.primarydocname = primarydocname;
    }

    public String getPreferredpharmacy() {
        return preferredpharmacy;
    }

    public void setPreferredpharmacy(String preferredpharmacy) {
        this.preferredpharmacy = preferredpharmacy;
    }

}
