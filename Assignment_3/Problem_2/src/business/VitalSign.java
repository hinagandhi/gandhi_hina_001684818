/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author user
 */
public class VitalSign {

    private int respiratoryrate;
    private int heartrate;
    private double systolicBP;
    private double weight;
    private String status;
    private Person person;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    private String Timestamp;
    private Date date;
    private int age;
    private Patient patient;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTimestamp() {
        Timestamp = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(date);
        return Timestamp;
    }

    public int getRespiratoryrate() {
        return respiratoryrate;
    }

    public void setRespiratoryrate(int respiratoryrate) {
        this.respiratoryrate = respiratoryrate;
    }

    public int getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(int heartrate) {
        this.heartrate = heartrate;
    }

    public double getSystolicBP() {
        return systolicBP;
    }

    public void setSystolicBP(double systolicBP) {
        this.systolicBP = systolicBP;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void statuscheck(Person person, VitalSign vs) {
        age = person.getAge();
        respiratoryrate = vs.getRespiratoryrate();
        heartrate = vs.getHeartrate();
        systolicBP = vs.getSystolicBP();
        weight = vs.getWeight();
        if (age >= 1 && age <= 3) {
            if ((respiratoryrate >= 20 && respiratoryrate <= 30) && (heartrate >= 80 && heartrate <= 130)
                    && (systolicBP >= 80 && systolicBP <= 110) && (weight >= 22 && weight <= 31)) {
                vs.setStatus("NORMAL");
            } else {
                vs.setStatus("ABNORMAL");
            }
        } else if (age >= 4 && age <= 5) {
            if ((respiratoryrate >= 20 && respiratoryrate <= 30) && (heartrate >= 80 && heartrate <= 120)
                    && (systolicBP >= 80 && systolicBP <= 110) && (weight >= 31 && weight <= 40)) {
                vs.setStatus("NORMAL");
            } else {
                vs.setStatus("ABNORMAL");
            }
        } else if (age >= 6 && age <= 12) {
            if ((respiratoryrate >= 20 && respiratoryrate <= 30) && (heartrate >= 70 && heartrate <= 110)
                    && (systolicBP >= 80 && systolicBP <= 120) && (weight >= 41 && weight <= 92)) {
                vs.setStatus("NORMAL");
            } else {
                vs.setStatus("ABNORMAL");
            }
        } else if (age >= 13 && age <= 150) {
            if ((respiratoryrate >= 12 && respiratoryrate <= 20) && (heartrate >= 55 && heartrate <= 105)
                    && (systolicBP >= 110 && systolicBP <= 120) && (weight > 110)) {
                vs.setStatus("NORMAL");
            } else {
                vs.setStatus("ABNORMAL");
            }
        }
    }

    @Override
    public String toString() {
        return this.getTimestamp(); //To change body of generated methods, choose Tools | Templates.
    }
}
