/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supplierdirectory;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class ProductCatalog {

    private ArrayList<ProductInformation> productCatalog;
    private ProductInformation productInformation;
    // private Supplier supplier;

    public ProductCatalog() {
        productCatalog = new ArrayList<>();
    }

    public ArrayList<ProductInformation> getArray() {
        return productCatalog;
    }

    //  public ProductInformation getProductInformation() {
    //    return productInformation;
    //}
    public ProductInformation addProduct() {
        productInformation = new ProductInformation();
        productCatalog.add(productInformation);
        return productInformation;
    }
}
