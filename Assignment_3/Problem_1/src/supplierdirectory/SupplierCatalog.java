/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supplierdirectory;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class SupplierCatalog {

    private ArrayList<Supplier> supplierCatalog;
    private Supplier supplier;

    public SupplierCatalog() {
        supplierCatalog = new ArrayList<>();
    }

    public ArrayList<Supplier> getSuppliers() {
        return supplierCatalog;
    }

    public void setSupplierCatalog(ArrayList<Supplier> supplierCatalog) {
        this.supplierCatalog = supplierCatalog;
    }

    public Supplier addSupplier() {
        supplier = new Supplier();
        supplierCatalog.add(supplier);
        return supplier;
    }

}
