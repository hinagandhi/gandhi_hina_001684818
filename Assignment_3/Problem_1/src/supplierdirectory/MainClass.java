/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supplierdirectory;

import java.util.Random;

/**
 *
 * @author user
 */
public class MainClass {

    public static void main(String args[]) {
        SupplierCatalog sc = new SupplierCatalog();
        Supplier dell = sc.addSupplier();
        Supplier hp = sc.addSupplier();
        Supplier apple = sc.addSupplier();
        Supplier lenevo = sc.addSupplier();
        Supplier toshiba = sc.addSupplier();
        dell.setSupplierName("DELL");
        hp.setSupplierName("HP");
        apple.setSupplierName("APPLE");
        lenevo.setSupplierName("LENEVO");
        toshiba.setSupplierName("TOSHIBA");
        String[] productName = {"Laptops", "Charger", "Tablets", "Servers", "Devices", "Battery", "Graphic", "Monitor", "Keypads", "Laptops"};
        String[] productModel = {"L11", "C12", "T13", "S14", "P15", "A16", "M17", "D18", "K19", "H20"};
        for (int k = 0; k <= 9; k++) {
            ProductInformation pDell = dell.getProductCatalog().addProduct();
            ProductInformation pHp = hp.getProductCatalog().addProduct();
            ProductInformation pApple = apple.getProductCatalog().addProduct();
            ProductInformation pLenevo = lenevo.getProductCatalog().addProduct();
            ProductInformation pToshiba = toshiba.getProductCatalog().addProduct();
            pDell.setProductName(productName[k]);
            pHp.setProductName(productName[k]);
            pApple.setProductName(productName[k]);
            pLenevo.setProductName(productName[k]);
            pToshiba.setProductName(productName[k]);
            pDell.setPID(productModel[k]);
            pHp.setPID(productModel[k]);
            pApple.setPID(productModel[k]);
            pLenevo.setPID(productModel[k]);
            pToshiba.setPID(productModel[k]);
            Random rn = new Random();
            double price = rn.nextInt(799) + 200;
            pDell.setProductPrice(price);
            pHp.setProductPrice(price);
            pApple.setProductPrice(price);
            pLenevo.setProductPrice(price);
            pToshiba.setProductPrice(price);
            Random rn1 = new Random();
            int qty = rn.nextInt(79) + 1;
            pDell.setQuantity(qty);
            pHp.setQuantity(qty);
            pApple.setQuantity(qty);
            pToshiba.setQuantity(qty);
            pLenevo.setQuantity(qty);
        }
        for (Supplier i : sc.getSuppliers()) {
            System.out.println(" ");
            System.out.println("                          " + i.getSupplierName());
            System.out.println("Name" + "              " + "Product ID" + "         " + "Price" + "         " + "Quantity");
            for (ProductInformation j : i.getProductCatalog().getArray()) {
                System.out.println(j.getProductName() + "            " + j.getPID() + "               " + j.getProductPrice() + "           " + j.getQuantity());
            }
        }
    }
}
