/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Role.NurseRole;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.LabTestWorkRequest;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Donor extends Employee {

   
   
 private String bloodType;
 
 private ArrayList<DonorHistory> donorAtributes;
 private String c = "D";
    public ArrayList<DonorHistory> getDonorAtributes() {
        return donorAtributes;
    }

    public void setDonorAtributes(ArrayList<DonorHistory> donorAtributes) {
        this.donorAtributes = donorAtributes;
    }
    public Donor()
    {
    donorAtributes = new ArrayList<>();
    DonorID = c+(int)(Math.random()*10000);
   
    }
    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
        
    }
 
 private Enterprise enterprise;

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }
 private UserAccount userAccount;
 private NurseRole nurseRole;

    public String getDonorID() {
        return DonorID;
    }

    public void setDonorID(String DonorID) {
        this.DonorID = DonorID;
    }
 private String DonorID;
 private static int count = 0;
   

    public NurseRole getNurseRole() {
        return nurseRole;
    }

    public void setNurseRole(NurseRole nurseRole) {
        this.nurseRole = nurseRole;
    }
    

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }
   /*  public String getID() {
        return DonorID;
    }*/

    @Override
    public String toString() {
        return DonorID; //To change body of generated methods, choose Tools | Templates.
    }
    
}
