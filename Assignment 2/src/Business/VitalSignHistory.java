/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
/**
 *
 * @author user
 */
import java.util.ArrayList;
public class VitalSignHistory {
   private ArrayList<VitalSign> vitalsignlist; 
   public VitalSignHistory()
   {
   vitalsignlist = new ArrayList<>();
   }
    public ArrayList<VitalSign> getVitalsignlist() {
        return vitalsignlist;
    }
    public VitalSign addVitalSign()
    {
    VitalSign vitalSign = new VitalSign();
    vitalsignlist.add(vitalSign);
    return vitalSign;
    }
    public void deleteVitalSign(VitalSign vitalSign)
    {
     vitalsignlist.remove(vitalSign);
    }
    public void clearVitalSign()
    {
    vitalsignlist.clear();
    }
}
