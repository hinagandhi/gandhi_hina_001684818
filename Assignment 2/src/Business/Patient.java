/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author user
 */
public class Patient {
private String patientfirstname;
private String patientlastname;
private String patientID;
private int age;
private String primarydocname;
private String preferredpharmacy;
private VitalSignHistory vitalsignhistory;
    public Patient()
    {
    vitalsignhistory = new VitalSignHistory();
    }
    
    public VitalSignHistory getVitalsignhistory() {
        return vitalsignhistory;
    }

    
    public String getPatientfirstname() {
        return patientfirstname;
    }

    public void setPatientfirstname(String patientfirstname) {
        this.patientfirstname = patientfirstname;
    }

    public String getPatientlastname() {
        return patientlastname;
    }

    public void setPatientlastname(String patientlastname) {
        this.patientlastname = patientlastname;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPrimarydocname() {
        return primarydocname;
    }

    public void setPrimarydocname(String primarydocname) {
        this.primarydocname = primarydocname;
    }

    public String getPreferredpharmacy() {
        return preferredpharmacy;
    }

    public void setPreferredpharmacy(String preferredpharmacy) {
        this.preferredpharmacy = preferredpharmacy;
    }
    
}
