/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BackEnd;
/**
 *
 * @author user
 */
public class Profile {
   private String first_name;
   private String last_name;
   private String middle_name;
   private String DOB_year;
   private String DOB_month;
   private String DOB_day;
   private String street_address;
   private String town;
   private String zipcode;
   private String occupation;
   private String email_address;
   private String area_code;
   private String phone_number;

    /**
     * @return the first_name
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     * @param first_name the first_name to set
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     * @return the last_name
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     * @param last_name the last_name to set
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     * @return the middle_name
     */
    public String getMiddle_name() {
        return middle_name;
    }

    /**
     * @param middle_name the middle_name to set
     */
    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    /**
     * @return the DOB_year
     */
    public String getDOB_year() {
        return DOB_year;
    }

    /**
     * @param DOB_year the DOB_year to set
     */
    public void setDOB_year(String DOB_year) {
        this.DOB_year = DOB_year;
    }

    /**
     * @return the DOB_month
     */
    public String getDOB_month() {
        return DOB_month;
    }

    /**
     * @param DOB_month the DOB_month to set
     */
    public void setDOB_month(String DOB_month) {
        this.DOB_month = DOB_month;
    }

    /**
     * @return the DOB_day
     */
    public String getDOB_day() {
        return DOB_day;
    }

    /**
     * @param DOB_day the DOB_day to set
     */
    public void setDOB_day(String DOB_day) {
        this.DOB_day = DOB_day;
    }

    /**
     * @return the street_address
     */
    public String getStreet_address() {
        return street_address;
    }

    /**
     * @param street_address the street_address to set
     */
    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @param town the town to set
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     * @return the zipcode
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * @param zipcode the zipcode to set
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     * @return the occupation
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * @param occupation the occupation to set
     */
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    /**
     * @return the email_address
     */
    public String getEmail_address() {
        return email_address;
    }

    /**
     * @param email_address the email_address to set
     */
    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    /**
     * @return the area_code
     */
    public String getArea_code() {
        return area_code;
    }

    /**
     * @param area_code the area_code to set
     */
    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }

    /**
     * @return the phone_number
     */
    public String getPhone_number() {
        return phone_number;
    }

    /**
     * @param phone_number the phone_number to set
     */
    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    
}
