/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class SalesPersonDirectory {
    private ArrayList<SalesPerson> salesPersonDirectory;
    private SalesPersonDirectory salesPersonD;
public SalesPersonDirectory()
{
salesPersonDirectory = new ArrayList<>();
}
    public ArrayList<SalesPerson> getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(ArrayList<SalesPerson> salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }
 
 public SalesPerson addSalesPerson()
 {
 SalesPerson salesPerson = new SalesPerson();
 salesPersonDirectory.add(salesPerson);
 return salesPerson;
 }
 public void removeSalesPerson(SalesPerson salesPerson)
 {
 salesPersonDirectory.remove(salesPerson);
 }
 public SalesPerson searchSalesPerson(String salesID)
 {
 for(SalesPerson p : salesPersonDirectory)
    {
    if(p.getSalesID().equalsIgnoreCase(salesID))
    {
    return p;
    }
    }
    return null;
    }
 public SalesPersonDirectory comparingSalesVolume(SalesPersonDirectory salesPersonDirec)
 {
 SalesPerson temp;
 for (int x=0; x<salesPersonDirec.getSalesPersonDirectory().size(); x++) // bubble sort outer loop
     {
 for (int i=0; i < salesPersonDirec.getSalesPersonDirectory().size()-1; i++) {
 if (salesPersonDirec.getSalesPersonDirectory().get(i).compareTo(salesPersonDirec.getSalesPersonDirectory().get(i+1)) < 0)
   {
temp = salesPersonDirec.getSalesPersonDirectory().get(i);
salesPersonDirec.getSalesPersonDirectory().set(i,salesPersonDirec.getSalesPersonDirectory().get(i+1) );
salesPersonDirec.getSalesPersonDirectory().set(i+1, temp);
                    }
                }
 }
 return salesPersonDirec;
 }
}