/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author user
 */
public class Customer {
  private String firstName;
  private String lastName;
  private String customerID;
  private int salesVolume;
  private SalesPerson salesPerson;
  private Product product;
  private OrderCatalog orderCatalog;

    public SalesPerson getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(SalesPerson salesPerson) {
        this.salesPerson = salesPerson;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public OrderCatalog getOrderCatalog() {
        return orderCatalog;
    }

    public void setOrderCatalog(OrderCatalog orderCatalog) {
        this.orderCatalog = orderCatalog;
    }
    public int getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(int salesVolume) {
        this.salesVolume = salesVolume;
    }
  public void calculateSalesVolume(OrderItem orderItem, Customer cust)
  {
  int oldsalesVolume = cust.getSalesVolume();
  int newsalesVolume = oldsalesVolume + orderItem.getQuantity();
  cust.setSalesVolume(newsalesVolume);
  }
  private static int count = 0;
  private String c = "C";
    public Customer()
    {
    customerID = c+(++count);
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }
    public int compareTo(Customer customer)
    {
    int r = 0;
    if(this.getSalesVolume() < customer.getSalesVolume())
        return -1;
    else if(this.getSalesVolume() < customer.getSalesVolume())
        return 1;
    else
        return 0;
    }
    @Override
    public String toString() {
        return this.getFirstName(); //To change body of generated methods, choose Tools | Templates.
    }
  
}
