/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author user
 */
public class Business {
 private CustomerDirectory customerDirectory;
 private SalesPersonDirectory salesPersonDirectory;
 private ProductCatalog productCatalog;

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }
 private MasterOrderCatalog masterOrderCatalog;

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public SalesPersonDirectory getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(SalesPersonDirectory salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }
 public static Business business;
 public static Business initialize()
 {
 if(business == null)
 {
    business = new Business();
  business.setInitialFile();
 }
 else
 {
     business = new Business();
     business.setInitialFile();
 }
 return business;
 }
 public Business()
 {
 
 customerDirectory = new CustomerDirectory();
 salesPersonDirectory = new SalesPersonDirectory();
 productCatalog = new ProductCatalog();
 masterOrderCatalog = new MasterOrderCatalog();
 
 }
 public void setInitialFile()
    {
    Product printer = productCatalog.addProduct();
    Product scanner = productCatalog.addProduct();
    printer.setProductName("Printer");
    scanner.setProductName("Scanner");
    printer.setCeilingPrice(120);
    scanner.setCeilingPrice(150);
    printer.setFloorPrice(90);
    scanner.setFloorPrice(100);
    printer.setTargetPrice(110);
    scanner.setTargetPrice(130);
    printer.setAvailability(100);
    scanner.setAvailability(200);
    Customer pacificGroup = customerDirectory.addCustomer();
    pacificGroup.setFirstName("pacific group");
    pacificGroup.setLastName("Resort");
    Customer cu = customerDirectory.addCustomer();
    cu.setFirstName("california state university");
    cu.setLastName("university");
    SalesPerson ram = salesPersonDirectory.addSalesPerson();
    ram.setFirstName("John");
    ram.setLastName("Smith");
    SalesPerson pa = salesPersonDirectory.addSalesPerson();
    pa.setFirstName("Lisa");
    pa.setLastName("Aziz");
    SalesPerson peter = salesPersonDirectory.addSalesPerson();
    peter.setFirstName("Peter");
    peter.setLastName("Jim");
    pa.setFloortotargetcommision(5);
    pa.setTargettoceilingcommision(10);
    ram.setFloortotargetcommision(5);
    ram.setTargettoceilingcommision(10);
    peter.setFloortotargetcommision(5);
    peter.setTargettoceilingcommision(10);
    }
}
