/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author user
 */
public class Product {
  private String productID;
  private double floorPrice;
  private double ceilingPrice;
  private double targetPrice;
  private String productName;
  private int availability;
  private int salesVolume;
    public int getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(int salesVolume) {
        this.salesVolume = salesVolume;
    }
  private static int countp = 1000;
  private String p = "P";
    public Product()
    {
    productID = p+(++countp);
    }
    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(double floorPrice) {
        this.floorPrice = floorPrice;
    }

    public double getCeilingPrice() {
        return ceilingPrice;
    }

    public void setCeilingPrice(double ceilingPrice) {
        this.ceilingPrice = ceilingPrice;
    }

    public double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(double targetPrice) {
        this.targetPrice = targetPrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }
    public void calculateSalesVolume(Product prod, OrderItem order)
    {
    int oldSalesVolume = prod.getSalesVolume();
    int newSalesVolume = oldSalesVolume + order.getQuantity();
    prod.setSalesVolume(newSalesVolume);
    }
    public int compareTo(Product product)
    {
    int r = 0;
    if(this.getSalesVolume() < product.getSalesVolume())
        return -1;
    else if(this.getSalesVolume() < product.getSalesVolume())
        return 1;
    else
        return 0;
    }
    @Override
    public String toString() {
        return this.getProductName(); //To change body of generated methods, choose Tools | Templates.
    }
  
}
