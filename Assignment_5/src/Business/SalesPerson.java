/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author user
 */
public class SalesPerson {

    private String firstName;
    private String lastName;
    private String salesID;
    private double floortotargetcommision;
    private double targettoceilingcommision;
    private int targetCount;
    private int belowTargetCount;
    private double indCommision;
    private int salesVolume;
    private double comm = 0.0;
    public void setIndCommision(double indCommision) {
        this.indCommision = indCommision;
    }

    public double getIndCommision() {
        return indCommision;
    }

  public double calculateIndCommision(Product prod, SalesPerson salesPerson, OrderItem oi) {
      double comm1 =0.0;
      
      if ((oi.getSalesPrice() <= prod.getCeilingPrice())&& (oi.getSalesPrice() > prod.getTargetPrice())) {
           comm1 =( (oi.getSalesPrice() * salesPerson.getTargettoceilingcommision())/100) * oi.getQuantity();             
    }
        else 
            if(oi.getSalesPrice() <= prod.getTargetPrice()&& oi.getSalesPrice() > prod.getFloorPrice())
        
        { comm1 = (oi.getSalesPrice() * salesPerson.getFloortotargetcommision())/100 * oi.getQuantity();
        }
      return comm1;
  }
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public OrderItem getOi() {
        return oi;
    }

    public void setOi(OrderItem oi) {
        this.oi = oi;
    }
    private Product product;
    private OrderItem oi;

    public int getBelowTargetCount() {
        return belowTargetCount;
    }

    public void setBelowTargetCount(int belowTargetCount) {
        this.belowTargetCount = belowTargetCount;
    }

    public int getTargetCount() {
        return targetCount;
    }

    public void setTargetCount(int targetCount) {
        this.targetCount = targetCount;
    }

    
    public double getFloortotargetcommision() {
        return floortotargetcommision;
    }

    public void setFloortotargetcommision(double floortotargetcommision) {
        this.floortotargetcommision = floortotargetcommision;
    }

    public double getTargettoceilingcommision() {
        return targettoceilingcommision;
    }

    public void setTargettoceilingcommision(double targettoceilingcommision) {
        this.targettoceilingcommision = targettoceilingcommision;
    }
    public int getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(int salesVolume) {
        this.salesVolume = salesVolume;
    }
    public String getSalesID() {
        return salesID;
    }
    private double commission;
    private String s = "S";
    private static int counts = 0;

    public SalesPerson() {
        salesID = s + (++counts);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }
  public void calculateSalesVolume(OrderItem orderItem, SalesPerson salesperson)
  {
  int oldsalesVolume = salesperson.getSalesVolume();
  int newsalesVolume = oldsalesVolume + orderItem.getQuantity();
  salesperson.setSalesVolume(newsalesVolume);
  }
   public void calculateCommission(Double salesPrice,Product product,SalesPerson salesPerson, int quantity) {
        comm = salesPerson.getCommission();
       if ((salesPrice <= product.getCeilingPrice())&& (salesPrice > product.getTargetPrice())) {
            comm = comm + ((salesPrice * (salesPerson.getTargettoceilingcommision()/100) )* quantity);
          //  salesVolume = salesVolume + quantity;
           salesPerson.setCommission(comm);
          //  salesPerson.setSalesVolume(salesVolume);
        } else if ((salesPrice <= product.getTargetPrice()) && (salesPrice > product.getFloorPrice())) {
            comm = comm + ((salesPrice * (salesPerson.getFloortotargetcommision() / 100) )* quantity);
          //  salesVolume = salesVolume + quantity;
            salesPerson.setCommission(comm);
          //  salesPerson.setSalesVolume(salesVolume);
                  } 
   }
    
    public void calculateCountTarget(OrderItem oi, SalesPerson salesPerson, Product product)
    {
     int old_count = salesPerson.getTargetCount();
     int new_count =0 ;
    if(oi.getSalesPrice() > product.getTargetPrice())
    {
    new_count = old_count + oi.getQuantity();
    salesPerson.setTargetCount(new_count);
    }
    }
    public void calculateCountBelowTarget(OrderItem oi, SalesPerson salesPerson, Product product)
    {
    int old_count = salesPerson.getBelowTargetCount();
    int new_count = 0 ;
    if(oi.getSalesPrice() < product.getTargetPrice())
    {
    new_count = old_count + oi.getQuantity();
    salesPerson.setBelowTargetCount(new_count);
    }
   
    }
    public int compareTo(SalesPerson sales)
    {
    int r = 0;
    if(this.getSalesVolume() < sales.getSalesVolume())
        return -1;
    else if(this.getSalesVolume() > sales.getSalesVolume())
        return 1;
    else
        return 0;
    }
    @Override
    public String toString() {
        return this.getFirstName(); //To change body of generated methods, choose Tools | Templates.
    }

}
