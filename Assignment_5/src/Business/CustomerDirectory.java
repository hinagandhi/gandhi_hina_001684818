/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class CustomerDirectory {
  private ArrayList<Customer> customerDirectory;  
  public CustomerDirectory()
  {
  customerDirectory = new ArrayList<>();
  }
    public ArrayList<Customer> getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(ArrayList<Customer> customerDirectory) {
        this.customerDirectory = customerDirectory;
    }
    public Customer addCustomer()
    {
    Customer customer = new Customer();
    customerDirectory.add(customer);
    return customer;
    }
    public void deleteCustomer(Customer customer)
    {
    customerDirectory.remove(customer);
    }
    public Customer searchCustomer(String CustomerID)
    {
    for(Customer c : customerDirectory)
    {
    if(c.getCustomerID().equalsIgnoreCase(CustomerID))
    {
    return c;
    }
    }
    return null;
    }
   public CustomerDirectory comparingSalesVolume(CustomerDirectory custDirec)
 {
 Customer temp;
 for (int x=0; x<custDirec.getCustomerDirectory().size(); x++) // bubble sort outer loop
     {
 for (int i=0; i < custDirec.getCustomerDirectory().size()-1; i++) {
 if (custDirec.getCustomerDirectory().get(i).compareTo(custDirec.getCustomerDirectory().get(i+1)) < 0)
   {
temp = custDirec.getCustomerDirectory().get(i);
custDirec.getCustomerDirectory().set(i,custDirec.getCustomerDirectory().get(i+1) );
custDirec.getCustomerDirectory().set(i+1, temp);
                    }
                }
 }
 return custDirec;
 }
}
