/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class ProductCatalog {
private ArrayList<Product> productCatalog;
private Product product;
public ProductCatalog()
{
productCatalog = new ArrayList<>();
}
    public ArrayList<Product> getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ArrayList<Product> productCatalog) {
        this.productCatalog = productCatalog;
    }
public Product addProduct()
{
product = new Product();
productCatalog.add(product);
return product;
}
public void removeProduct(Product product)
{
productCatalog.remove(product);
}
public Product searchProduct(String ProductID)
{
for(Product p : productCatalog)
    {
    if(p.getProductID().equals(ProductID))
    {
    return p;
    }
    }
    return null;
}
public ProductCatalog comparingSalesVolumeProduct(ProductCatalog product)
 {
     Product temp;
 for (int x=0; x<product.getProductCatalog().size(); x++) // bubble sort outer loop
            {
                for (int i=0; i < product.getProductCatalog().size()-1; i++) {
                    if (product.getProductCatalog().get(i).compareTo(product.getProductCatalog().get(i+1)) < 0)
                    {
                        temp = product.getProductCatalog().get(i);
                        product.getProductCatalog().set(i,product.getProductCatalog().get(i+1) );
                        product.getProductCatalog().set(i+1, temp);
                    }
                }
 }
 return product;
 }
}
