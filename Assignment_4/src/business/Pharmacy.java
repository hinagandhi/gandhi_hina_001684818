/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;
/**
 *
 * @author user
 */
 public class Pharmacy {
 private String pharmacyName;
 private String headquarters;
 private PharmacyDirectory pharmacyDirectory;
 private Pharmacy pharmacy;
 private DrugCatalog drugCatalog;
 private String sequence ;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
 public Pharmacy()
 {
 drugCatalog = new DrugCatalog();
 }
 public DrugCatalog getDrugCatalog() {
        return drugCatalog;
    }
    public String getPharmacyName() {
        return pharmacyName;
    }
    
    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    }

    public String getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(String headquarters) {
        this.headquarters = headquarters;
    }
  
    @Override
    public String toString() {
        return this.getPharmacyName(); //To change body of generated methods, choose Tools | Templates.
    }

}
