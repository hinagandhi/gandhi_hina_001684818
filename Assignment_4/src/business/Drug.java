/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.Date;

/**
 *
 * @author user
 */
 public class Drug {
 private DrugCatalog drugCatalog;
 private String drugName;
 private DrugItem drugItem;
    public Drug()
    {
    drugItem = new DrugItem();
    }
    public DrugItem getDrugItem() {
        return drugItem;
    }

    public void setDrugItem(DrugItem drugItem) {
        this.drugItem = drugItem;
    }
    
    public String getDose() {
        return dose;
    }

    //private String drugSerialNumber;
    public void setDose(String dose) {
        this.dose = dose;
    }
 private String dose;
 private String expirationDate;

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
 //private double sellingPrice;
 private String description;
 //private int quantity;
 private String warning; 

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }
 private String drugComponent;
 private String use;

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

    public String getDrugComponent() {
        return drugComponent;
    }

    public void setDrugComponent(String drugComponent) {
        this.drugComponent = drugComponent;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    @Override
    public String toString() {
        return this.getDrugName(); //To change body of generated methods, choose Tools | Templates.
    }
 
}
