/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author user
 */
public class Store {
 private String street;
 private String storeLocation; 
 private String storeOwner;
 private DrugInventory drugInventory;

    public DrugSupplier getDrugSupplier() {
        return drugSupplier;
    }

    public void setDrugSupplier(DrugSupplier drugSupplier) {
        this.drugSupplier = drugSupplier;
    }
    private DrugSupplier drugSupplier;
    public DrugInventory getDrugInventory() {
        return drugInventory;
    }

    public void setDrugInventory(DrugInventory drugInventory) {
        this.drugInventory = drugInventory;
    }
   public Store()
   {
   drugInventory = new DrugInventory(); 
   }
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    
    }
 private String StoreID;

    public String getStoreID() {
        return StoreID;
    }

    public void setStoreID(String StoreID) {
        this.StoreID = StoreID;
    }
    public String getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    public String getStoreOwner() {
        return storeOwner;
    }

    public void setStoreOwner(String storeOwner) {
        this.storeOwner = storeOwner;
    }
    
    @Override
    public String toString() {
        return this.getStoreID(); //To change body of generated methods, choose Tools | Templates.
    }
 
}
