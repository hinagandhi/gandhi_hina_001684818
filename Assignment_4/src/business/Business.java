/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author user
 */
public class Business {
private Pharmacy pharmacy;
private DrugCatalog drugCatalog;
    public PharmacyDirectory getPharmacyDirectory() {
        return pharmacyDirectory;
    }

    public void setPharmacyDirectory(PharmacyDirectory pharmacyDirectory) {
        this.pharmacyDirectory = pharmacyDirectory;
    }

    public StoreDirectory getStoreDirectory() {
        return storeDirectory;
    }

    //private StoreDirectory storeInventory;
    public void setStoreDirectory(StoreDirectory storeDirectory) {
        this.storeDirectory = storeDirectory;
    }
  private PharmacyDirectory pharmacyDirectory;
  private StoreDirectory storeDirectory;
  private MaterDrugCatalog masterDrugCatalog;

    public MaterDrugCatalog getMasterDrugCatalog() {
        return masterDrugCatalog;
    }

    public void setMasterDrugCatalog(MaterDrugCatalog masterDrugCatalog) {
        this.masterDrugCatalog = masterDrugCatalog;
    }
  public Business()
  {
  pharmacyDirectory = new PharmacyDirectory();
  storeDirectory = new StoreDirectory();
   //pharmacyDirectory.pharmacyList();
  masterDrugCatalog = new MaterDrugCatalog();
  }
  
    public void pharmacyList()
    {
    //String[] pharmaList = {"Pfizer","Cipla"};
   // String[] sequence1 = {"a","b"};
    String[] drugName = {"acupril","Amitriptyline","benadryl" };
    String[] drugDose = {"20, 40, or 80 mg/day","10 mg ; 50 mg ; 75 mg","300 mg"};
    String[] expirationDate = {"10/11/2017","10/11/2018","10/11/2016"};
    String[] description = {"Accupril (quinapril) is an ACE inhibitor","Amitriptyline is a tricyclic antidepressant"
    ,"blocks the effects of the naturally occurring chemical histamine in the body","cure cold cough and fever"};
    String[] component = { "salt","furazolidone","diphenydramine"};
    String[] warning = {"Use is not recommended during the first trimester of pregnancy","You should not use this medication if you are allergic to amitriptyline"
    ,"Do not take Benadryl if you have taken a monoamine oxidase inhibitor"};
    String[] use = {"Accupril is used to treat high blood pressure (hypertension) and heart failure","antidepressant","Benadryl is also used to suppress coughs, to treat motion sickness, to induce sleep, and to treat mild forms of Parkinson's disease"};
    String[] drugNameCipla = {"Adapalene","Amoxicillin","Crocin" };
    String[] drugDoseCipla = {"20mg","10 mg","300 mg"};
    String[] expirationDateCipla = {"10/11/2017","10/11/2018","10/11/2016"};
    String[] descriptionCipla = {"Adapalene (quinapril) is an ACE inhibitor","Amitriptyline is a tricyclic antidepressant"
    ,"blocks the effects of the naturally occurring chemical histamine in the body","cure cold cough and fever"};
    String[] componentCipla = { "salt","furazolidone","diphenydramine"};
    String[] warningCipla = {"Use is not recommended during the first trimester of pregnancy","You should not use this medication if you are allergic to amitriptyline"
    ,"Do not take Benadryl if you have taken a monoamine oxidase inhibitor"};
    String[] useCipla = {"Adapalene is used to treat high blood pressure (hypertension) and heart failure","antidepressant","crocin removes headache"};
    Pharmacy pharmacyPfizer = pharmacyDirectory.addPharmacy();
    Pharmacy pharmacyCipla = pharmacyDirectory.addPharmacy();
    pharmacyPfizer.setPharmacyName("Pfizer");
    pharmacyCipla.setPharmacyName("Cipla");
    
    //pharmacyCipla.setSequence("b");
    for(int j = 0; j<drugName.length; j++)
    {
    Drug drug = pharmacyPfizer.getDrugCatalog().addDrug();
    Drug drugC = pharmacyCipla.getDrugCatalog().addDrug();
    drug.setDrugName(drugName[j]);
    drug.setDescription(description[j]);
    drug.setDose(drugDose[j]);
    drug.setDrugComponent(component[j]);
    drug.setExpirationDate(expirationDate[j]);
    drug.setWarning(warning[j]);
    drug.setUse(use[j]);
    drugC.setDrugName(drugNameCipla[j]);
    drugC.setDescription(descriptionCipla[j]);
    drugC.setDose(drugDoseCipla[j]);
    drugC.setDrugComponent(componentCipla[j]);
    drugC.setExpirationDate(expirationDateCipla[j]);
    drugC.setWarning(warningCipla[j]);
    drugC.setUse(useCipla[j]);
    }
    }
    }
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author SONY
 */


if (!pm.getUsername_pattern().matcher(nameJTextField.getText()).matches()) {
            JOptionPane.showMessageDialog(this, "Enter valid username. Allows username with length at least 3 to max 16 and there "
                    + "should be only lowercase letters, numbers or underscore in the username.", "Caution", JOptionPane.ERROR_MESSAGE);
            return;

        } else {
            userName = nameJTextField.getText();
        }

