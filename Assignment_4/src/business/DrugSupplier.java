/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author user
 */
public class DrugSupplier {
   int quantity;
   private Drug drug;
   private Pharmacy pharmacy;
   private DrugItem drugItem;
   private Store store;

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
  public DrugSupplier()
  {
  drug = new Drug();
  pharmacy = new Pharmacy();
  drugItem = new DrugItem();
  }
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public Pharmacy getPharmacy() {
        return pharmacy;
    }

    public void setPharmacy(Pharmacy pharmacy) {
        this.pharmacy = pharmacy;
    }

    public DrugItem getDrugItem() {
        return drugItem;
    }

    public void setDrugItem(DrugItem drugItem) {
        this.drugItem = drugItem;
    }

    @Override
    public String toString() {
        return this.getDrug().getDrugName(); //To change body of generated methods, choose Tools | Templates.
    }
}
